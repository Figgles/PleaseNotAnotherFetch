#!/bin/bash
readarray -t ascii < "$(dirname $0)/ascii.txt"
memUsed=$(grep 'Active' /proc/meminfo -m1 | sed 's/.*:\s*//')
memTotal=$(grep 'MemTotal' /proc/meminfo -m1 | sed 's/.*:\s*//')
echo -e "$(printf '%-20s' "${ascii[0]}") \x1B[1;31musr\x1B[0m: $(whoami) | $(cat /etc/hostname)"
echo -e "$(printf '%-20s' "${ascii[1]}") \x1B[1;33mos \x1B[0m: $(grep 'ID' /etc/os-release -m1 | sed 's/.*=//')"
echo -e "$(printf '%-20s' "${ascii[2]}") \x1B[1;32msh \x1B[0m: $(basename $SHELL)"
echo -e "$(printf '%-20s' "${ascii[3]}") \x1B[1;36mup \x1B[0m: $(uptime -p | sed 's/up\s*//')"
echo -e "$(printf '%-20s' "${ascii[4]}") \x1B[1;34mkrn\x1B[0m: $(uname -r)"
echo -e "$(printf '%-20s' "${ascii[5]}") \x1B[1;35mcpu\x1B[0m: $(grep 'model name' /proc/cpuinfo -m1 | sed 's/.*:\s*//')"
echo -e "$(printf '%-20s' "${ascii[6]}") \x1B[1;31mgpu\x1B[0m: $(lspci | grep -i vga | sed 's/.*\[//; s/\].*//')"
echo -e "$(printf '%-20s' "${ascii[7]}") \x1B[1;33mmem\x1B[0m: ${memUsed} / ${memTotal}"
